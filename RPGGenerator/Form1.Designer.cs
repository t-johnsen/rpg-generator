﻿namespace RPGGenerator
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cbCharactersChoise = new System.Windows.Forms.ComboBox();
            this.tbName = new System.Windows.Forms.TextBox();
            this.tbArmor = new System.Windows.Forms.TextBox();
            this.tbHP = new System.Windows.Forms.TextBox();
            this.tbEnergy = new System.Windows.Forms.TextBox();
            this.tbHeight = new System.Windows.Forms.TextBox();
            this.lblName = new System.Windows.Forms.Label();
            this.lblArmor = new System.Windows.Forms.Label();
            this.lblHP = new System.Windows.Forms.Label();
            this.lblEnergy = new System.Windows.Forms.Label();
            this.lblHeight = new System.Windows.Forms.Label();
            this.btnCharacter = new System.Windows.Forms.Button();
            this.lblSelectedCharacter = new System.Windows.Forms.Label();
            this.lbSummary = new System.Windows.Forms.ListBox();
            this.lbExistingCharacters = new System.Windows.Forms.ListBox();
            this.btnDeleteCharacter = new System.Windows.Forms.Button();
            this.lblCharacterInfo = new System.Windows.Forms.Label();
            this.btnEditExistingCharacter = new System.Windows.Forms.Button();
            this.lblExistingCharacter = new System.Windows.Forms.Label();
            this.lblCharacterEdit = new System.Windows.Forms.Label();
            this.btnEditCharacter = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // cbCharactersChoise
            // 
            this.cbCharactersChoise.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbCharactersChoise.FormattingEnabled = true;
            this.cbCharactersChoise.Location = new System.Drawing.Point(670, 85);
            this.cbCharactersChoise.Name = "cbCharactersChoise";
            this.cbCharactersChoise.Size = new System.Drawing.Size(100, 25);
            this.cbCharactersChoise.TabIndex = 0;
            // 
            // tbName
            // 
            this.tbName.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbName.Location = new System.Drawing.Point(670, 116);
            this.tbName.Name = "tbName";
            this.tbName.Size = new System.Drawing.Size(100, 25);
            this.tbName.TabIndex = 1;
            // 
            // tbArmor
            // 
            this.tbArmor.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbArmor.Location = new System.Drawing.Point(670, 148);
            this.tbArmor.Name = "tbArmor";
            this.tbArmor.Size = new System.Drawing.Size(100, 25);
            this.tbArmor.TabIndex = 2;
            // 
            // tbHP
            // 
            this.tbHP.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbHP.Location = new System.Drawing.Point(670, 179);
            this.tbHP.Name = "tbHP";
            this.tbHP.Size = new System.Drawing.Size(100, 25);
            this.tbHP.TabIndex = 3;
            // 
            // tbEnergy
            // 
            this.tbEnergy.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbEnergy.Location = new System.Drawing.Point(670, 210);
            this.tbEnergy.Name = "tbEnergy";
            this.tbEnergy.Size = new System.Drawing.Size(100, 25);
            this.tbEnergy.TabIndex = 4;
            // 
            // tbHeight
            // 
            this.tbHeight.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbHeight.Location = new System.Drawing.Point(670, 241);
            this.tbHeight.Name = "tbHeight";
            this.tbHeight.Size = new System.Drawing.Size(100, 25);
            this.tbHeight.TabIndex = 5;
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblName.Location = new System.Drawing.Point(562, 116);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(51, 17);
            this.lblName.TabIndex = 6;
            this.lblName.Text = "Name:";
            // 
            // lblArmor
            // 
            this.lblArmor.AutoSize = true;
            this.lblArmor.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblArmor.Location = new System.Drawing.Point(562, 148);
            this.lblArmor.Name = "lblArmor";
            this.lblArmor.Size = new System.Drawing.Size(52, 17);
            this.lblArmor.TabIndex = 7;
            this.lblArmor.Text = "Armor:";
            // 
            // lblHP
            // 
            this.lblHP.AutoSize = true;
            this.lblHP.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHP.Location = new System.Drawing.Point(562, 179);
            this.lblHP.Name = "lblHP";
            this.lblHP.Size = new System.Drawing.Size(32, 17);
            this.lblHP.TabIndex = 8;
            this.lblHP.Text = "HP:";
            // 
            // lblEnergy
            // 
            this.lblEnergy.AutoSize = true;
            this.lblEnergy.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEnergy.Location = new System.Drawing.Point(562, 210);
            this.lblEnergy.Name = "lblEnergy";
            this.lblEnergy.Size = new System.Drawing.Size(58, 17);
            this.lblEnergy.TabIndex = 9;
            this.lblEnergy.Text = "Energy:";
            // 
            // lblHeight
            // 
            this.lblHeight.AutoSize = true;
            this.lblHeight.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHeight.Location = new System.Drawing.Point(562, 241);
            this.lblHeight.Name = "lblHeight";
            this.lblHeight.Size = new System.Drawing.Size(45, 17);
            this.lblHeight.TabIndex = 10;
            this.lblHeight.Text = "Hight:";
            // 
            // btnCharacter
            // 
            this.btnCharacter.BackColor = System.Drawing.Color.LimeGreen;
            this.btnCharacter.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCharacter.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnCharacter.Location = new System.Drawing.Point(565, 363);
            this.btnCharacter.Name = "btnCharacter";
            this.btnCharacter.Size = new System.Drawing.Size(205, 34);
            this.btnCharacter.TabIndex = 11;
            this.btnCharacter.Text = "Submit new character";
            this.btnCharacter.UseVisualStyleBackColor = false;
            this.btnCharacter.Click += new System.EventHandler(this.btnCharacter_Click);
            // 
            // lblSelectedCharacter
            // 
            this.lblSelectedCharacter.AutoSize = true;
            this.lblSelectedCharacter.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSelectedCharacter.Location = new System.Drawing.Point(562, 85);
            this.lblSelectedCharacter.Name = "lblSelectedCharacter";
            this.lblSelectedCharacter.Size = new System.Drawing.Size(84, 17);
            this.lblSelectedCharacter.TabIndex = 12;
            this.lblSelectedCharacter.Text = "Select type:";
            // 
            // lbSummary
            // 
            this.lbSummary.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbSummary.FormattingEnabled = true;
            this.lbSummary.ItemHeight = 17;
            this.lbSummary.Location = new System.Drawing.Point(213, 81);
            this.lbSummary.Name = "lbSummary";
            this.lbSummary.Size = new System.Drawing.Size(156, 276);
            this.lbSummary.TabIndex = 13;
            // 
            // lbExistingCharacters
            // 
            this.lbExistingCharacters.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbExistingCharacters.FormattingEnabled = true;
            this.lbExistingCharacters.ItemHeight = 17;
            this.lbExistingCharacters.Location = new System.Drawing.Point(27, 81);
            this.lbExistingCharacters.Name = "lbExistingCharacters";
            this.lbExistingCharacters.Size = new System.Drawing.Size(161, 276);
            this.lbExistingCharacters.TabIndex = 14;
            this.lbExistingCharacters.SelectedIndexChanged += new System.EventHandler(this.lbExistingCharacters_SelectedIndexChanged);
            // 
            // btnDeleteCharacter
            // 
            this.btnDeleteCharacter.BackColor = System.Drawing.Color.OrangeRed;
            this.btnDeleteCharacter.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeleteCharacter.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnDeleteCharacter.Location = new System.Drawing.Point(27, 363);
            this.btnDeleteCharacter.Name = "btnDeleteCharacter";
            this.btnDeleteCharacter.Size = new System.Drawing.Size(161, 34);
            this.btnDeleteCharacter.TabIndex = 15;
            this.btnDeleteCharacter.Text = "Delete";
            this.btnDeleteCharacter.UseVisualStyleBackColor = false;
            this.btnDeleteCharacter.Click += new System.EventHandler(this.btnDeleteCharacter_Click);
            // 
            // lblCharacterInfo
            // 
            this.lblCharacterInfo.AutoSize = true;
            this.lblCharacterInfo.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCharacterInfo.Location = new System.Drawing.Point(210, 61);
            this.lblCharacterInfo.Name = "lblCharacterInfo";
            this.lblCharacterInfo.Size = new System.Drawing.Size(100, 17);
            this.lblCharacterInfo.TabIndex = 16;
            this.lblCharacterInfo.Text = "Character info";
            // 
            // btnEditExistingCharacter
            // 
            this.btnEditExistingCharacter.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.btnEditExistingCharacter.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEditExistingCharacter.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnEditExistingCharacter.Location = new System.Drawing.Point(213, 363);
            this.btnEditExistingCharacter.Name = "btnEditExistingCharacter";
            this.btnEditExistingCharacter.Size = new System.Drawing.Size(156, 34);
            this.btnEditExistingCharacter.TabIndex = 17;
            this.btnEditExistingCharacter.Text = "Edit information";
            this.btnEditExistingCharacter.UseVisualStyleBackColor = false;
            this.btnEditExistingCharacter.Click += new System.EventHandler(this.btnEditExistingCharacter_Click);
            // 
            // lblExistingCharacter
            // 
            this.lblExistingCharacter.AutoSize = true;
            this.lblExistingCharacter.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblExistingCharacter.Location = new System.Drawing.Point(24, 61);
            this.lblExistingCharacter.Name = "lblExistingCharacter";
            this.lblExistingCharacter.Size = new System.Drawing.Size(134, 17);
            this.lblExistingCharacter.TabIndex = 18;
            this.lblExistingCharacter.Text = "Existing characters";
            // 
            // lblCharacterEdit
            // 
            this.lblCharacterEdit.AutoSize = true;
            this.lblCharacterEdit.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCharacterEdit.Location = new System.Drawing.Point(562, 61);
            this.lblCharacterEdit.Name = "lblCharacterEdit";
            this.lblCharacterEdit.Size = new System.Drawing.Size(73, 17);
            this.lblCharacterEdit.TabIndex = 19;
            this.lblCharacterEdit.Text = "Character";
            // 
            // btnEditCharacter
            // 
            this.btnEditCharacter.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.btnEditCharacter.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEditCharacter.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnEditCharacter.Location = new System.Drawing.Point(565, 323);
            this.btnEditCharacter.Name = "btnEditCharacter";
            this.btnEditCharacter.Size = new System.Drawing.Size(205, 34);
            this.btnEditCharacter.TabIndex = 20;
            this.btnEditCharacter.Text = "Edit existing character";
            this.btnEditCharacter.UseVisualStyleBackColor = false;
            this.btnEditCharacter.Click += new System.EventHandler(this.btnEditCharacter_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btnEditCharacter);
            this.Controls.Add(this.lblCharacterEdit);
            this.Controls.Add(this.lblExistingCharacter);
            this.Controls.Add(this.btnEditExistingCharacter);
            this.Controls.Add(this.lblCharacterInfo);
            this.Controls.Add(this.btnDeleteCharacter);
            this.Controls.Add(this.lbExistingCharacters);
            this.Controls.Add(this.lbSummary);
            this.Controls.Add(this.lblSelectedCharacter);
            this.Controls.Add(this.btnCharacter);
            this.Controls.Add(this.lblHeight);
            this.Controls.Add(this.lblEnergy);
            this.Controls.Add(this.lblHP);
            this.Controls.Add(this.lblArmor);
            this.Controls.Add(this.lblName);
            this.Controls.Add(this.tbHeight);
            this.Controls.Add(this.tbEnergy);
            this.Controls.Add(this.tbHP);
            this.Controls.Add(this.tbArmor);
            this.Controls.Add(this.tbName);
            this.Controls.Add(this.cbCharactersChoise);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cbCharactersChoise;
        private System.Windows.Forms.TextBox tbName;
        private System.Windows.Forms.TextBox tbArmor;
        private System.Windows.Forms.TextBox tbHP;
        private System.Windows.Forms.TextBox tbEnergy;
        private System.Windows.Forms.TextBox tbHeight;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Label lblArmor;
        private System.Windows.Forms.Label lblHP;
        private System.Windows.Forms.Label lblEnergy;
        private System.Windows.Forms.Label lblHeight;
        private System.Windows.Forms.Button btnCharacter;
        private System.Windows.Forms.Label lblSelectedCharacter;
        private System.Windows.Forms.ListBox lbSummary;
        private System.Windows.Forms.ListBox lbExistingCharacters;
        private System.Windows.Forms.Button btnDeleteCharacter;
        private System.Windows.Forms.Label lblCharacterInfo;
        private System.Windows.Forms.Button btnEditExistingCharacter;
        private System.Windows.Forms.Label lblExistingCharacter;
        private System.Windows.Forms.Label lblCharacterEdit;
        private System.Windows.Forms.Button btnEditCharacter;
    }
}

