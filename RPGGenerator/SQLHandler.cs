﻿using RPGLiberary;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGGenerator
{
    /*
     * Class handling sql connection and CRUD operations
     */
    class SQLHandler
    {

        private SqlConnection Connect()
        {
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
            builder.DataSource = "PC7375\\SQLEXPRESS";
            builder.InitialCatalog = "RPGDatabase";
            builder.IntegratedSecurity = true;

            return new SqlConnection(builder.ConnectionString);
        }

        // Read all characters from database
        public List<Character> ReadCharacters(out Dictionary<int, int> types)
        {
            string sql = "SELECT * FROM character";
            List<Character> characters = new List<Character>();
            types = new Dictionary<int, int>();
            try
            {
                using (SqlConnection connection = Connect())
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())

                            {
                                characters.Add(new Character(reader.GetInt32(0), reader.GetString(1), reader.GetInt32(2), reader.GetInt32(3), reader.GetInt32(4), Convert.ToDouble(reader.GetDecimal(5))));
                                Console.WriteLine(reader.GetInt32(0) + " **** " +  reader.GetInt32(6));
                                types.Add(reader.GetInt32(0), reader.GetInt32(6));
                            }
                        }
                    }
                }
                return characters;
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.Message);
            }

            return null;
        }

        //Create new character in database
        public int InsertCharacter(string name, int hp, int energy, int armor, double hight, int type)
        {
            string sql = "INSERT INTO character(name, hp, energy, armor, hight, type) output INSERTED.characterID VALUES(@name, @hp, @energy, @armor, @hight, @type)";
            try
            {
                using (SqlConnection connection = Connect())
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@name", name);
                        command.Parameters.AddWithValue("@hp", hp);
                        command.Parameters.AddWithValue("@energy", energy);
                        command.Parameters.AddWithValue("@armor", armor);
                        command.Parameters.AddWithValue("@hight", hight);
                        command.Parameters.AddWithValue("@type", type);

                        return (int)command.ExecuteScalar();
                    }
                }
                
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.Message);
            }

            return -1;
        }

        // Delete character from database
        public void DeleteCharacter(int id)
        {
            string sql = "DELETE FROM character WHERE characterID = @id";
            try
            {
                using (SqlConnection connection = Connect())
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@id", id);
                        command.ExecuteNonQuery();
                    }
                }

            }
            catch (SqlException e)
            {
                Console.WriteLine(e.Message);
                
            }
        }

        // Update character in database
        public void UpdateCharacter(int characterID, string name, int hp, int energy, int armor, double hight, int type)
        {
            string sql = "UPDATE character SET name = @name, hp = @hp, energy = @energy, armor = @armor, hight = @hight, type = @type WHERE characterID = @characterID";
            try
            {
                using (SqlConnection connection = Connect())
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@name", name);
                        command.Parameters.AddWithValue("@hp", hp);
                        command.Parameters.AddWithValue("@energy", energy);
                        command.Parameters.AddWithValue("@armor", armor);
                        command.Parameters.AddWithValue("@hight", hight);
                        command.Parameters.AddWithValue("@type", type);
                        command.Parameters.AddWithValue("@characterID", characterID);

                        command.ExecuteNonQuery();
                    }
                }

            }
            catch (SqlException e)
            {
                Console.WriteLine(e.Message);
            }

        }


    }
}
