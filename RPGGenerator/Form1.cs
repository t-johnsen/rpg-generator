﻿using RPGLiberary;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RPGGenerator
{
    public partial class Form1 : Form
    {
        #region variables
        SQLHandler handler = null;
        Dictionary<int, int> types = null;
        List<Character> characters = null;
        #endregion

        public Form1()
        {
            InitializeComponent();
            handler = new SQLHandler();
            characters = handler.ReadCharacters(out types);
        }

        /*
         * Loading the form and initilizing misc
         */
        private void Form1_Load(object sender, EventArgs e)
        {

            List<string> typeNames = new List<string>() { "Elf", "Urukhai", "Man", "Nazgul" };

            cbCharactersChoise.DataSource = new BindingSource(typeNames, null);

            foreach (Character character in characters) lbExistingCharacters.Items.Add(character);

            cbCharactersChoise.SelectedIndex = -1;

            btnEditCharacter.Enabled = false;


        }

        /*
         * Creates new character and updates database and internal Character-list
         */
        private void btnCharacter_Click(object sender, EventArgs e)
        {
            lbSummary.Items.Clear();
            if (!GetValuesFromInputFields(out int characterOptionNumber, out string name, out int HP, out int armor, out int energy, out double hight)) return;

            int insertedID = handler.InsertCharacter(name, HP, energy, armor, hight, characterOptionNumber);

            UpdateInternalCharacters(insertedID, characterOptionNumber, name, HP, energy, armor, hight);
            MessageBox.Show($"Your character, {name}, was created successfully!");

        }

        /*
         * Displays chosen character from existing characters in listbox
         */
        private void lbExistingCharacters_SelectedIndexChanged(object sender, EventArgs e)
        {
            Console.WriteLine(lbExistingCharacters.SelectedIndex);
            if (lbExistingCharacters.SelectedIndex != -1)
            {
                Character selected = (Character)lbExistingCharacters.SelectedItem;
                AddToSummary(selected);
                ClearTextBoxesForDataInsertion();

            }
        }
        /*
         * Puts information from character object into editable fields when pressing "Edit" button beneath "Character summary"
         */
        private void btnEditExistingCharacter_Click(object sender, EventArgs e)
        {
            btnEditCharacter.Enabled = true;
            Character selected = (Character)lbExistingCharacters.SelectedItem;
            tbName.Text = selected.Name;
            cbCharactersChoise.SelectedIndex = types[selected.CharacterID];
            tbHP.Text = Convert.ToString(selected.HP);
            tbArmor.Text = Convert.ToString(selected.Armor);
            tbEnergy.Text = Convert.ToString(selected.Energy);
            tbHeight.Text = Convert.ToString(selected.Hight);
        }

        /*
         * Button action for submitting edited information and updating existing character in database and internal
         */
        private void btnEditCharacter_Click(object sender, EventArgs e)
        {
            if (!GetValuesFromInputFields(out int characterOptionNumber, out string name, out int HP, out int armor, out int energy, out double hight)) return;

            lbSummary.Items.Clear();
            Character selected = (Character)lbExistingCharacters.SelectedItem;
            characters.Remove(selected);

            lbExistingCharacters.ClearSelected();
            lbExistingCharacters.Items.Remove(selected);

            types.Remove(selected.CharacterID);
            UpdateInternalCharacters(selected.CharacterID, characterOptionNumber, name, HP, energy, armor, hight);
            handler.UpdateCharacter(selected.CharacterID, name, HP, energy, armor, hight, characterOptionNumber);

            MessageBox.Show($"Your character, {name}, was successfully updated!");
            btnEditCharacter.Enabled = false;
        }

        /*
         * Deliting character from database and from internal list
         */
        private void btnDeleteCharacter_Click(object sender, EventArgs e)
        {
            Character selected = (Character)lbExistingCharacters.SelectedItem;
            lbExistingCharacters.ClearSelected();
            handler.DeleteCharacter(selected.CharacterID);
            lbExistingCharacters.Items.Remove(selected);
            lbSummary.Items.Clear();
            types.Remove(selected.CharacterID);
            characters.Remove(selected);
        }

        /*
         * Helper method:
         * Checks all inputfields for content. Returns false if not filled in.
         */
        private bool GetValuesFromInputFields(out int characterOptionNumber, out string name, out int HP, out int armor, out int energy, out double hight)
        {
            if(tbName.Text.ToString() == "" || tbName.Text.ToString() == null || cbCharactersChoise.SelectedIndex == -1 || tbHP.Text == null || tbHP.Text == ""
                || tbArmor.Text == "" || tbArmor.Text == null || tbEnergy.Text == null || tbEnergy.Text == "" || tbEnergy.Text == null || tbHeight.Text == null || tbHeight.Text == "")
            {
                MessageBox.Show("Please fill inn all the fields before submitting!");
                characterOptionNumber = 0;
                HP = 0;
                armor = 0;
                energy = 0;
                name = "";
                hight = 0;
                return false;
            }
            name = tbName.Text.ToString();
            characterOptionNumber = cbCharactersChoise.SelectedIndex;
            HP = int.Parse(tbHP.Text);
            armor = int.Parse(tbArmor.Text);
            energy = int.Parse(tbEnergy.Text);
            hight = double.Parse(tbHeight.Text);
            return true;
        }

        /*
         * Fills text fields in summary box 
         */
        private void AddToSummary(Character character)
        {
            if (lbExistingCharacters.Items.Count != 0)
            {
                lbSummary.Items.Clear();
                lbSummary.Items.Add($"Type:\t{FindType(character.CharacterID)}");
                lbSummary.Items.Add($"Name:\t{character.Name}");
                lbSummary.Items.Add($"HP:\t{character.HP}");
                lbSummary.Items.Add($"Energy:\t{character.Energy}");
                lbSummary.Items.Add($"Armor:\t{character.Armor}");
                lbSummary.Items.Add($"Hight:\t{character.Hight}");
            }
        }

        /*
         * Used to both update and create new character.
         * Creates new character based on character option choise.
         * Clears fields and updates internal lists.
         */
        private void UpdateInternalCharacters(int insertedID, int characterOptionNumber, string name, int HP, int energy, int armor, double hight)
        {

            switch (characterOptionNumber)
            {
                case 0:
                    characters.Add(new Elf(insertedID, name, HP, energy, armor, hight));
                    break;
                case 1:
                    characters.Add(new Uruk_hai(insertedID, name, HP, energy, armor, hight));
                    break;
                case 2:
                    characters.Add(new Man(insertedID, name, HP, energy, armor, hight));
                    break;
                case 3:
                    characters.Add(new Nazgul(insertedID, name, HP, energy, armor, hight));
                    break;
                default:
                    //Do something if error happens
                    break;
            }

            Character character = characters.Find(n => n.CharacterID == insertedID);
            types.Add(insertedID, characterOptionNumber);
            lbExistingCharacters.Items.Clear();
            foreach (Character x in characters) lbExistingCharacters.Items.Add(x);
            AddToSummary(character);
            ClearTextBoxesForDataInsertion();

        }

        /*
         * "Stringify" character option from combobox
         */
        private string FindType(int id)
        {
            Console.WriteLine(id + " -- " + types[id]);
            Console.WriteLine(types[id]);
            switch (types[id])
            {
                case 0:
                    return "Elf";
                case 1:
                    return "Urukhai";
                case 2:
                    return "Man";
                case 3:
                    return "Nazgul";
                default:
                    break;
            }
            return "No type";
        }

        /*
         * Clears all text boxes for data
         */
        private void ClearTextBoxesForDataInsertion()
        {
            cbCharactersChoise.SelectedIndex = -1;
            tbName.Clear();
            tbArmor.Clear();
            tbHP.Clear();
            tbEnergy.Clear();
            tbHeight.Clear();
        }

        /*
         * Write to .txt file. Currently not used.
         */
        private void WriteToFile(Character character)
        {
            try
            {
                using (StreamWriter writer = new StreamWriter(@"RPGCharacters.txt", true))
                {
                    writer.WriteLine($"{character.Name}, {character.HP}, {character.Energy}, {character.Armor}, {character.Hight} ");
                }
            }
            catch (Exception e)
            {

                Console.WriteLine(e.Message);
            }

        }

    }

}
