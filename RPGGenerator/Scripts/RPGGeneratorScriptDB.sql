USE [RPGDatabase]
GO

/****** Object:  Table [dbo].[character]    Script Date: 8/21/2020 4:15:33 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[character]') AND type in (N'U'))
DROP TABLE [dbo].[character]
GO

/****** Object:  Table [dbo].[character]    Script Date: 8/21/2020 4:15:33 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[character](
	[characterID] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NOT NULL,
	[hp] [int] NOT NULL,
	[energy] [int] NOT NULL,
	[armor] [int] NOT NULL,
	[hight] [decimal](18, 0) NOT NULL,
	[type] [int] NOT NULL,
 CONSTRAINT [PK_character] PRIMARY KEY CLUSTERED 
(
	[characterID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

