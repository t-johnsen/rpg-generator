USE [master]
GO
/****** Object:  Database [RPGDatabase]    Script Date: 8/21/2020 4:24:58 PM ******/
CREATE DATABASE [RPGDatabase]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'RPGDatabase', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\RPGDatabase.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'RPGDatabase_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\RPGDatabase_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [RPGDatabase] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [RPGDatabase].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [RPGDatabase] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [RPGDatabase] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [RPGDatabase] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [RPGDatabase] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [RPGDatabase] SET ARITHABORT OFF 
GO
ALTER DATABASE [RPGDatabase] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [RPGDatabase] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [RPGDatabase] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [RPGDatabase] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [RPGDatabase] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [RPGDatabase] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [RPGDatabase] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [RPGDatabase] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [RPGDatabase] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [RPGDatabase] SET  DISABLE_BROKER 
GO
ALTER DATABASE [RPGDatabase] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [RPGDatabase] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [RPGDatabase] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [RPGDatabase] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [RPGDatabase] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [RPGDatabase] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [RPGDatabase] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [RPGDatabase] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [RPGDatabase] SET  MULTI_USER 
GO
ALTER DATABASE [RPGDatabase] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [RPGDatabase] SET DB_CHAINING OFF 
GO
ALTER DATABASE [RPGDatabase] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [RPGDatabase] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [RPGDatabase] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [RPGDatabase] SET QUERY_STORE = OFF
GO
USE [RPGDatabase]
GO
/****** Object:  Table [dbo].[character]    Script Date: 8/21/2020 4:24:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[character](
	[characterID] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NOT NULL,
	[hp] [int] NOT NULL,
	[energy] [int] NOT NULL,
	[armor] [int] NOT NULL,
	[hight] [decimal](18, 0) NOT NULL,
	[type] [int] NOT NULL,
 CONSTRAINT [PK_character] PRIMARY KEY CLUSTERED 
(
	[characterID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[character] ON 

INSERT [dbo].[character] ([characterID], [name], [hp], [energy], [armor], [hight], [type]) VALUES (17, N'Legolas', 3, 3, 3, CAST(3 AS Decimal(18, 0)), 0)
INSERT [dbo].[character] ([characterID], [name], [hp], [energy], [armor], [hight], [type]) VALUES (25, N'SomeOtherDude', 8, 8, 8, CAST(8 AS Decimal(18, 0)), 0)
INSERT [dbo].[character] ([characterID], [name], [hp], [energy], [armor], [hight], [type]) VALUES (26, N'SomeDudette', 8, 8, 43, CAST(8 AS Decimal(18, 0)), 0)
INSERT [dbo].[character] ([characterID], [name], [hp], [energy], [armor], [hight], [type]) VALUES (30, N'Voff', 8, 8, 8, CAST(8 AS Decimal(18, 0)), 0)
INSERT [dbo].[character] ([characterID], [name], [hp], [energy], [armor], [hight], [type]) VALUES (38, N'DummyDataSucks', 9, 3, 9, CAST(1 AS Decimal(18, 0)), 0)
INSERT [dbo].[character] ([characterID], [name], [hp], [energy], [armor], [hight], [type]) VALUES (40, N'AnotherOne?', 9, 9, 9, CAST(0 AS Decimal(18, 0)), 0)
INSERT [dbo].[character] ([characterID], [name], [hp], [energy], [armor], [hight], [type]) VALUES (41, N'OhWell', 8, 8, 8, CAST(8 AS Decimal(18, 0)), 2)
SET IDENTITY_INSERT [dbo].[character] OFF
GO
USE [master]
GO
ALTER DATABASE [RPGDatabase] SET  READ_WRITE 
GO
