# CRUD application
### RPG Generator with simple GUI where you can create your characters
Allows user to create and edit characters from [RPG Library] (class library) with Windomws Forms.
Connect to a database to run the CRUD-operations through the SQLHandler class.

[//]: #
[RPG Library]: <https://gitlab.com/t-johnsen/rpg-generator-library>